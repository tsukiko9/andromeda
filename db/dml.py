# -*- coding: utf-8 -*-
# @Time           : 2018/9/30 下午3:04
# @Author         : Cirno
# @File           : dml.py
# @Description    ：
from db.sqlite import DBHelper
from lib.logger import logger


def get_goods_list():
    logger('正在读取商品URL列表...')

    _dml = 'SELECT URL FROM GOODS'
    _result = DBHelper().execute(_dml)
    if _result is not None:
        logger('读取商品URL列表，成功', 's')
        return _result
    else:
        logger('读取商品URL列表，异常', 'e')


def update_goods_price(goods_list):
    _dml = "UPDATE GOODS SET TITLE = '{0}', CURRENT = '{1}', LOWER = '{2}' WHERE URL = ?".format(
        goods_list['title'], goods_list['current'], goods_list['lower'])
    logger('正在更新商品价格...执行sql :' + _dml, 'd')
    _result = DBHelper().execute(_dml, goods_list['url'], is_many=False, is_commit=True)
    logger('执行商品更新操作完成...')

    # 执行史低批处理
    update_price_lower()


def update_price_before():
    # 更新前一日价格
    logger('价格数据更新前处理准备...')

    _dml = 'UPDATE GOODS SET LAST = CURRENT'
    _result = DBHelper().execute(_dml, is_commit=True)
    logger('价格数据更新前处理完毕...')


def update_price_lower():
    # 更新史低价格
    logger('正在批处理史低价格...')

    _dml = 'UPDATE GOODS SET LOWER = CURRENT WHERE CURRENT < LOWER'
    _result = DBHelper().execute(_dml, is_commit=True)
    logger('批处理史低价格完毕...')


def get_price_change_list():
    logger('正在获取价格变动商品列表')

    _dml = 'SELECT A.URL URL, A.CURRENT CURRENT, ' \
           'A.LAST LAST, A.LOWER LOWER, A.TITLE TITLE, ' \
           'A.LAST - A.CURRENT DIFF_LAST, ' \
           'B.JOIN_PRICE - A.CURRENT DIFF_CAR, ' \
           'B.USER_ID USER_ID, ' \
           'B.JOIN_PRICE JOIN_PRICE ' \
           'FROM GOODS A, USER_GOODS B ' \
           'WHERE A.URL = B.GOODS_URL AND A.CURRENT < A.LAST'

    _result = DBHelper().execute(_dml, is_dict=True)
    if _result is not None:
        logger('读取推送用户列表，成功', 's')
        return _result
    else:
        logger('无可推送用户')


def delete_push_list(msg_type):
    logger('正在删除推送表，类别【{0}】...'.format(msg_type))

    _dml = 'DELETE FROM PUSH_LIST WHERE TYPE = ?'
    _result = DBHelper().execute(_dml, msg_type, is_commit=True)


def insert_push_list(push_list):
    logger('正在插入推送表...')
    _dml = 'INSERT INTO PUSH_LIST(USER_ID, MESSAGES, TYPE) VALUES(?, ?, ?)'
    _result = DBHelper().execute(_dml, params_list=push_list, is_many=True, is_commit=True)
