# -*- coding: utf-8 -*-
# @Time           : 2018/9/30 下午2:45
# @Author         : Cirno
# @File           : core.py
# @Description    ：
from events.jd import jd_price
from lib.logger import logger


def core():
    logger('正在启动调度器...')
    # 启动调度器
    scheduler()


def scheduler():
    jd_price()
