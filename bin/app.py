# -*- coding: utf-8 -*-
# @Time           : 2018/9/30 上午11:06
# @Author         : Cirno
# @File           : app.py
# @Description    ：
import tornado

from tornado import ioloop
from apscheduler.schedulers.tornado import TornadoScheduler

from bin.core import core
from lib.logger import logger


def main():
    scheduler = TornadoScheduler()
    # scheduler.add_job(core, 'interval', minutes=1)
    scheduler.add_job(core, 'cron', day_of_week='0-6', hour='10', minute='5')
    scheduler.add_job(core, 'cron', day_of_week='0-6', hour='16', minute='30')
    # 服务启动
    logger('Andromeda 服务启动...')
    scheduler.start()
    try:
        tornado.ioloop.IOLoop.current().start()
    except (KeyboardInterrupt, SystemExit) as e:
        scheduler.shutdown()
        logger('服务关闭{0}'.format(e))
