# -*- coding: utf-8 -*-
# @Time           : 2018/9/30 上午10:42
# @Author         : Cirno
# @File           : start.py
# @Description    ：main
from bin.app import main

if __name__ == '__main__':
    main()
