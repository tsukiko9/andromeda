# -*- coding: utf-8 -*-
# @Time           : 2018/9/30 下午1:38
# @Author         : Cirno
# @File           : conf.py
# @Description    ：

import yaml


class GlobalConf(object):
    def __init__(self):
        self.__conf_file = yaml.load(open('./conf/conf.yaml'))
        self.__global_conf = self.__conf_file['Global']
        self.db_path = self.__global_conf['DBPath']
        self.is_debug = self.__global_conf['DeBug']


class ProxyConf(object):
    def __init__(self):
        self.__conf_file = yaml.load(open('./conf/conf.yaml'))
        self.__proxy_conf = self.__conf_file['Proxy']
        self.host = self.__proxy_conf['Host']
        self.port = self.__proxy_conf['Port']
        self.headers = self.__proxy_conf['Headers']
