# Andromeda 数据调度器

## **简介**

Andromeda负责 ExCream的数据源获取部分。

## **依赖**

* Tornado
* APScheduler
* js2py
* pyyaml
* sqlite

## **目录结构**

```
.
├── Pipfile
├── README.md
├── bin
│   ├── __init__.py
│   ├── app.py
│   └── core.py
├── conf
│   └── conf.yaml
├── db
│   ├── __init__.py
│   ├── dml.py
│   └── sqlite.py
├── events
│   ├── __init__.py
│   └── jd.py
├── lib
│   ├── __init__.py
│   ├── logger.py
│   └── net.py
├── log.log
├── module
│   ├── __init__.py
│   └── conf.py
└── start.py

```
