# -*- coding: utf-8 -*-
# @Time           : 2018/10/1 下午10:30
# @Author         : Cirno
# @File           : net.py
# @Description    ：
import json
from time import sleep

from tornado import httpclient

from lib.logger import logger
from module.conf import ProxyConf


class SyncClient:

    def __init__(self, retry=3):
        self._retry = retry

    def exec(self, url, proxy=True):
        # 设置使用代理参数
        httpclient.AsyncHTTPClient.configure(
            "tornado.curl_httpclient.CurlAsyncHTTPClient"
        )
        # 启动代理模式
        if proxy:
            __proxy = ProxyConf()
            client = httpclient.HTTPClient()
            for i in range(self._retry):
                try:
                    sleep(2)
                    rep = client.fetch(url, method='GET', headers=__proxy.headers, proxy_host=__proxy.host,
                                       proxy_port=__proxy.port, connect_timeout=25.0, request_timeout=30.0
                                       )
                    if rep.code == 200:
                        data_json = json.loads(rep.body.decode("gb2312"))
                        logger('抓取数据成功，数据为：{0}'.format(data_json), 'd')
                        return data_json
                    else:
                        logger('代理ISP提供商响应错误，正在重试第{0}次...'.format(i + 1), 'w')
                        sleep(5)
                        continue

                    pass

                except Exception as e:
                    logger('数据抓取异常，正在重试第{0}次，错误信息：{1}'.format(i + 1, e), 'e')
                    sleep(5)
                    continue
